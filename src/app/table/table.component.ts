import { Component, OnInit } from '@angular/core';
import {MatrixService} from '../matrix.service'

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  rows: number = 4;
  columns: number = 3; 
  constructor(private matrixService: MatrixService) {
  //  console.log([].constructor(this.rows));
  this.matrixService.rows = this.rows;
  this.matrixService.columns = this.columns;
  this.matrixService.createMatrix();
  console.log(this.matrixService.matrix);
  }

  ngOnInit(): void {
  }
  arrayOne(n: number): any[] {
    console.log(n);
    return Array(n);
  }

}
