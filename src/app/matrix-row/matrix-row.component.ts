import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-matrix-row',
  templateUrl: './matrix-row.component.html',
  styleUrls: ['./matrix-row.component.css']
})
export class MatrixRowComponent implements OnInit {
  @Input() rows: number;
  @Input() columns: number;
  @Input() row:Number;


  constructor() { }
  
  ngOnInit(): void {
  }
  arrayOne(n: number): any[] {
    console.log(n);
    return Array(n);
  }  


}
