import { Component, OnInit,Output, EventEmitter, Input } from '@angular/core';
import { MatrixService } from '../matrix.service'

@Component({
  selector: 'app-table-cell',
  templateUrl: './table-cell.component.html',
  styleUrls: ['./table-cell.component.css']
})
export class TableCellComponent implements OnInit {

  @Input() rowNumber:number;
  @Input() colNumber:number;
  @Input() row:number;
  @Input() col:number;
  @Output() mark = new EventEmitter<any>();
  isBlocked:boolean = false;

  constructor(private matrixService: MatrixService) { 
    console.log(this.matrixService.matrix);
    console.log(this.isBlocked)
  }
 
  ngOnInit(): void {
  }

  Block(){
    this.matrixService.matrix[this.row][this.col] = 1 - this.matrixService.matrix[this.row][this.col];
    this.matrixService.printMatrix();
    this.isBlocked = !this.isBlocked;
    }

}
