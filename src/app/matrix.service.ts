import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MatrixService {

  matrix:any;
  rows:number;
  columns:number;
  constructor() {
    console.log("created");
  }
  createMatrix(){
    this.matrix = new Array(this.rows).fill(0).map(() => new Array(this.columns).fill(0));
  }
  printMatrix() {
    console.log(this.matrix);
  }


}
