import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { TableComponent } from './table/table.component';
import { MatrixRowComponent } from './matrix-row/matrix-row.component';
import { TableCellComponent } from './table-cell/table-cell.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TableComponent,
    MatrixRowComponent,
    TableCellComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
